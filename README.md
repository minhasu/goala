# goala

> My praiseworthy Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## Goala Project
*目標管理アプリ*
- 目標管理が、便利に、楽しく、簡単にできるようにする
- Goalaを通して成長を実感できる

### 機能郡
- 目標一覧機能
  - タスク一覧が閲覧できること
  - 曜日ごとにタブで切り替えれること
  - 目標追加ボタンがあり、作成ページに遷移できること
- 目標詳細機能
  - 設定した目標が閲覧できること
- 目標設定機能
  - 目標内容が設定できること
    - ゆくゆくは大目標→中目標→小目標（タスク単位）に落とせるように
  - 目標時間が設定できること
- 結果評価機能
  - 投資時間を入力できること
    - 目標時間と結果時間の比較
  - 満足度を入力できること
    - 0〜200点で自己評価
  - メモを入力できること
- 集計機能
  - 目標ごとに、合計時間、満足度平均が閲覧できること
- ユーザー登録機能
  - Google連携ができること
  - ユーザー名が設定・編集できること
  - パスワードが設定・編集できること
  - iconが設定・編集できること
- Firebase
  - users
  - goal
  - result